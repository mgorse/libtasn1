|Branch|CI system|Status|
|:----:|:-------:|-----:|
|Master|Gitlab|[![build status](https://gitlab.com/gnutls/libtasn1/badges/master/build.svg)](https://gitlab.com/gnutls/libtasn1/commits/master)|

# libtasn1

This is GNU Libtasn1, a small ASN.1 library.

The C library (libtasn1.*) is licensed under the GNU Lesser General
Public License version 2.1 or later.  See the file COPYING.LIB.

The command line tool, self tests, examples, and other auxilliary
files, are licensed under the GNU General Public License version 3.0
or later.  See the file COPYING.


## Manual

The manual is in the `doc/` directory of the release.  You can also browse
the manual online at:

https://gnutls.gitlab.io/libtasn1/


## Homepage

The project homepage at the gnu site is at:

http://www.gnu.org/software/libtasn1/


For any copyright year range specified as YYYY-ZZZZ in this package
note that the range specifies every single year in that closed interval.
